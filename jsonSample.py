#!/usr/bin/python
import random
import copy



jsonSample = []
  
  
# record 0
record = {
"guid": "a7bc132e-ca7e-465c-a5ad-baf24ec4cd29",
"isActive": "true",
"balance": "$1,191.17",
"picture": "http://placehold.it/32x32",
"age": 37,
"eyeColor": "brown",
"name": "Griffith Sosa",
"gender": "male",
"company": "EMERGENT",
"email": "griffithsosa@emergent.com",
"phone": "+1 (965) 432-2891",
"address": "654 Lawton Street, Rehrersburg, Palau, 1027",
"about": "Ex tempor ut cupidatat eu ut labore qui ut. Sit proident et non esse voluptate eu laborum minim nisi do. Minim mollit laborum ea irure sit esse aliquip sunt dolor cupidatat commodo dolor qui eu. Lorem incididunt minim laborum dolor occaecat fugiat. Elit deserunt laborum qui sint eiusmod exercitation proident ipsum.\r\n",
"registered": "2016-03-25T04:01:02 -01:00",
"latitude": -34.427211,
"longitude": -136.14106,
"tags": [
  "labore",
  "ullamco",
  "qui",
  "consectetur",
  "reprehenderit",
  "sit",
  "anim"
],
"friends": [
  {
    "id": 0,
    "name": "Olson Boyer"
  },
  {
    "id": 1,
    "name": "Leonard Clay"
  },
  {
    "id": 2,
    "name": "Karen Booth"
  }
],
"greeting": "Hello, Griffith Sosa! You have 2 unread messages.",
"favoriteFruit": "apple"
}

jsonSample.append(record)

# record 1
record = {
"guid": "26ddfe37-4efb-4b22-a38a-cc3dae892861",
"isActive": "false",
"balance": "$3,283.18",
"picture": "http://placehold.it/32x32",
"age": 40,
"eyeColor": "blue",
"name": "Blackwell Cardenas",
"gender": "male",
"company": "EXOSWITCH",
"email": "blackwellcardenas@exoswitch.com",
"phone": "+1 (864) 473-2195",
"address": "132 Tabor Court, Saranap, Oklahoma, 2796",
"about": "Commodo occaecat nulla qui sint. In quis enim magna irure duis consequat eu eiusmod Lorem non voluptate laborum. Ut ipsum minim aute dolore consequat ad aute incididunt est veniam. Veniam dolore Lorem esse proident eu irure. Culpa reprehenderit esse est officia sint dolore consequat amet aute aliqua amet. Reprehenderit laborum commodo irure amet.\r\n",
"registered": "2014-11-10T01:59:39 -01:00",
"latitude": 1.190903,
"longitude": 106.221805,
"tags": [
  "tempor",
  "non",
  "voluptate",
  "enim",
  "tempor",
  "cillum",
  "cupidatat"
],
"friends": [
  {
    "id": 0,
    "name": "Mercedes Ruiz"
  },
  {
    "id": 1,
    "name": "Webster Perry"
  },
  {
    "id": 2,
    "name": "Bridgett Rush"
  }
],
"greeting": "Hello, Blackwell Cardenas! You have 10 unread messages.",
"favoriteFruit": "strawberry"
}

jsonSample.append(record)

# record 2
record = {
"guid": "3983cee9-c4da-4880-8586-00daa1d0a32a",
"isActive": "true",
"balance": "$1,699.49",
"picture": "http://placehold.it/32x32",
"age": 27,
"eyeColor": "brown",
"name": "Genevieve Waters",
"gender": "female",
"company": "GEOFORMA",
"email": "genevievewaters@geoforma.com",
"phone": "+1 (816) 495-3984",
"address": "701 Porter Avenue, Bancroft, Virgin Islands, 229",
"about": "Lorem est duis dolore tempor velit ullamco nostrud commodo ex occaecat ex. Non occaecat cupidatat amet incididunt non et. Magna magna dolore veniam sunt sit non minim laboris.\r\n",
"registered": "2016-01-13T10:54:07 -01:00",
"latitude": -44.792853,
"longitude": 122.359538,
"tags": [
  "id",
  "ipsum",
  "proident",
  "sint",
  "dolor",
  "ut",
  "dolore"
],
"friends": [
  {
    "id": 0,
    "name": "Sexton Petty"
  },
  {
    "id": 1,
    "name": "Moody Downs"
  },
  {
    "id": 2,
    "name": "Graham Potter"
  }
],
"greeting": "Hello, Genevieve Waters! You have 3 unread messages.",
"favoriteFruit": "strawberry"
}

jsonSample.append(record)

# record 3
record = {
"guid": "6b1753ce-c196-42bf-91b4-0347c9cea3e0",
"isActive": "true",
"balance": "$3,234.04",
"picture": "http://placehold.it/32x32",
"age": 25,
"eyeColor": "green",
"name": "Davis Cummings",
"gender": "male",
"company": "GLUID",
"email": "daviscummings@gluid.com",
"phone": "+1 (898) 551-3672",
"address": "352 Willoughby Avenue, Brooktrails, Texas, 703",
"about": "Id sint labore nulla Lorem commodo elit consequat laboris nisi officia. Dolor in qui cillum ipsum commodo adipisicing ipsum non nostrud consequat nulla Lorem non quis. Sint sit cupidatat et qui ad proident officia in et. Exercitation esse labore do laborum qui proident est eiusmod consectetur sunt. Amet nulla Lorem est ut et qui velit cupidatat aliquip mollit ad. Ad deserunt reprehenderit voluptate anim voluptate adipisicing voluptate quis velit.\r\n",
"registered": "2015-04-01T04:22:42 -02:00",
"latitude": -40.583408,
"longitude": -57.372466,
"tags": [
  "ad",
  "voluptate",
  "eiusmod",
  "eiusmod",
  "laboris",
  "aliqua",
  "proident"
],
"friends": [
  {
    "id": 0,
    "name": "Christensen Burris"
  },
  {
    "id": 1,
    "name": "Ana Chase"
  },
  {
    "id": 2,
    "name": "Traci Carson"
  }
],
"greeting": "Hello, Davis Cummings! You have 3 unread messages.",
"favoriteFruit": "banana"
}

jsonSample.append(record)

# record 4
record = {
"guid": "9d71a3dc-a876-4d8e-98ee-8baebd744cdf",
"isActive": "true",
"balance": "$3,228.60",
"picture": "http://placehold.it/32x32",
"age": 32,
"eyeColor": "brown",
"name": "Coleen Campos",
"gender": "female",
"company": "ZILLAR",
"email": "coleencampos@zillar.com",
"phone": "+1 (970) 560-2307",
"address": "352 Creamer Street, Lisco, Utah, 5654",
"about": "Elit dolore commodo veniam nisi eu adipisicing. Nostrud excepteur aute ipsum esse ex est officia elit aliqua incididunt sint fugiat laboris. Ut cupidatat eu duis voluptate dolore officia cupidatat voluptate velit excepteur ad dolor. Proident quis magna mollit nostrud adipisicing amet laboris non. Irure pariatur dolore duis aute aliqua fugiat veniam consequat exercitation nisi. Consequat magna magna ipsum laborum dolor excepteur laboris est ullamco.\r\n",
"registered": "2014-07-29T07:56:10 -02:00",
"latitude": 77.46129,
"longitude": 53.979,
"tags": [
  "Lorem",
  "duis",
  "aliquip",
  "nostrud",
  "adipisicing",
  "tempor",
  "aliqua"
],
"friends": [
  {
    "id": 0,
    "name": "Isabel Larsen"
  },
  {
    "id": 1,
    "name": "Lillie Landry"
  },
  {
    "id": 2,
    "name": "Aline Mcdowell"
  }
],
"greeting": "Hello, Coleen Campos! You have 9 unread messages.",
"favoriteFruit": "apple"
}

jsonSample.append(record)

# record 5
record = {
"guid": "5d9aca01-6780-42d2-a3aa-c1b43ef7dfeb",
"isActive": "true",
"balance": "$2,423.33",
"picture": "http://placehold.it/32x32",
"age": 24,
"eyeColor": "blue",
"name": "Francis Booker",
"gender": "female",
"company": "VORATAK",
"email": "francisbooker@voratak.com",
"phone": "+1 (811) 405-2154",
"address": "656 Brightwater Court, Detroit, North Dakota, 9738",
"about": "Quis est amet sunt elit veniam est commodo id nulla. Non sint esse id excepteur eu quis duis elit mollit. Pariatur id sunt deserunt et proident minim aliqua. In commodo eiusmod ullamco commodo ullamco amet fugiat Lorem officia. Deserunt deserunt commodo eu mollit ipsum Lorem in cupidatat. Veniam eu ea veniam veniam quis ea.\r\n",
"registered": "2014-04-11T12:43:28 -02:00",
"latitude": -75.105658,
"longitude": -132.372772,
"tags": [
  "anim",
  "est",
  "deserunt",
  "laborum",
  "sit",
  "adipisicing",
  "nostrud"
],
"friends": [
  {
    "id": 0,
    "name": "Jocelyn Vincent"
  },
  {
    "id": 1,
    "name": "Tabatha Smith"
  },
  {
    "id": 2,
    "name": "Georgette Huber"
  }
],
"greeting": "Hello, Francis Booker! You have 5 unread messages.",
"favoriteFruit": "banana"
}

jsonSample.append(record)

# record 6
record = {
"guid": "cb6cd9a4-6d53-455f-9eb4-1fbed9fc7127",
"isActive": "true",
"balance": "$2,606.32",
"picture": "http://placehold.it/32x32",
"age": 26,
"eyeColor": "green",
"name": "Barnes Clements",
"gender": "male",
"company": "ATGEN",
"email": "barnesclements@atgen.com",
"phone": "+1 (847) 447-3494",
"address": "743 Covert Street, Day, New Jersey, 8410",
"about": "Voluptate enim nostrud laboris dolor eu veniam aute culpa dolore. Magna adipisicing eiusmod eiusmod nostrud id consequat. Eiusmod est mollit velit cillum consectetur. Non culpa pariatur irure ex proident cillum velit velit veniam veniam. Quis nisi ullamco aliqua proident veniam.\r\n",
"registered": "2016-03-09T07:51:26 -01:00",
"latitude": -83.766472,
"longitude": -142.968708,
"tags": [
  "mollit",
  "eu",
  "pariatur",
  "irure",
  "Lorem",
  "aliquip",
  "excepteur"
],
"friends": [
  {
    "id": 0,
    "name": "Elizabeth Rose"
  },
  {
    "id": 1,
    "name": "Darla Colon"
  },
  {
    "id": 2,
    "name": "Diaz Wheeler"
  }
],
"greeting": "Hello, Barnes Clements! You have 2 unread messages.",
"favoriteFruit": "strawberry"
}

jsonSample.append(record)

# record 7
record = {
"guid": "fb7c2ade-a310-4bd1-b0ee-cd946cbca29d",
"isActive": "true",
"balance": "$1,897.88",
"picture": "http://placehold.it/32x32",
"age": 37,
"eyeColor": "green",
"name": "Lilian Thornton",
"gender": "female",
"company": "POOCHIES",
"email": "lilianthornton@poochies.com",
"phone": "+1 (822) 532-2985",
"address": "268 Grattan Street, Delco, Alaska, 4990",
"about": "Qui sunt occaecat pariatur ea laboris culpa tempor veniam culpa velit pariatur. Nostrud exercitation dolore aliqua sint reprehenderit quis fugiat est labore. Proident dolore exercitation aute deserunt ullamco labore laboris officia dolore. Veniam consectetur velit commodo ipsum sit non. In tempor sit tempor veniam commodo proident consequat ullamco excepteur laborum pariatur. Mollit excepteur incididunt do minim cupidatat amet id nulla eu voluptate adipisicing duis.\r\n",
"registered": "2015-10-17T05:50:46 -02:00",
"latitude": -60.707983,
"longitude": -95.664937,
"tags": [
  "non",
  "aliquip",
  "excepteur",
  "Lorem",
  "laborum",
  "et",
  "anim"
],
"friends": [
  {
    "id": 0,
    "name": "Kayla Buchanan"
  },
  {
    "id": 1,
    "name": "Benton Saunders"
  },
  {
    "id": 2,
    "name": "Verna Todd"
  }
],
"greeting": "Hello, Lilian Thornton! You have 10 unread messages.",
"favoriteFruit": "banana"
}

jsonSample.append(record)

# record 8
record = {
"guid": "56bb11af-c0d6-4e86-ad3f-3cbc8026cb98",
"isActive": "true",
"balance": "$1,117.73",
"picture": "http://placehold.it/32x32",
"age": 40,
"eyeColor": "green",
"name": "Sampson Collins",
"gender": "male",
"company": "TROPOLIS",
"email": "sampsoncollins@tropolis.com",
"phone": "+1 (896) 589-3617",
"address": "942 Fair Street, Carbonville, Michigan, 3357",
"about": "Veniam ipsum ad elit aliqua. Elit irure mollit elit aliqua irure laboris anim qui eu velit officia ullamco. Ea enim ut ex adipisicing consequat. Tempor voluptate id veniam mollit magna occaecat nisi ipsum quis veniam. Ad reprehenderit tempor qui laborum nisi pariatur pariatur nostrud id nulla incididunt esse dolor in.\r\n",
"registered": "2016-07-20T12:34:22 -02:00",
"latitude": -83.75279,
"longitude": 160.011217,
"tags": [
  "dolore",
  "enim",
  "ullamco",
  "sunt",
  "mollit",
  "fugiat",
  "dolore"
],
"friends": [
  {
    "id": 0,
    "name": "Ebony Casey"
  },
  {
    "id": 1,
    "name": "Potts Austin"
  },
  {
    "id": 2,
    "name": "Augusta Reeves"
  }
],
"greeting": "Hello, Sampson Collins! You have 9 unread messages.",
"favoriteFruit": "apple"
}

jsonSample.append(record)

# record 9
record = {
"guid": "0dc6851e-945b-4fbc-9096-7c28eb6411a1",
"isActive": "true",
"balance": "$1,013.35",
"picture": "http://placehold.it/32x32",
"age": 29,
"eyeColor": "brown",
"name": "Romero Cervantes",
"gender": "male",
"company": "FREAKIN",
"email": "romerocervantes@freakin.com",
"phone": "+1 (994) 408-3736",
"address": "468 Quentin Street, Shepardsville, Kentucky, 8472",
"about": "Nisi ex aute sit eiusmod aute minim anim aute ex amet dolore minim cillum quis. Non aute amet ullamco excepteur et occaecat nulla est. Culpa quis elit eu do commodo. Enim adipisicing non amet ipsum aliquip fugiat pariatur reprehenderit cupidatat. Ullamco nostrud commodo laboris quis commodo velit et est magna laboris dolor. Eiusmod labore consequat et commodo exercitation esse duis quis deserunt consequat pariatur. Ut amet eu veniam non ad adipisicing deserunt aute do qui ipsum eu aliqua proident.\r\n",
"registered": "2014-12-11T10:09:36 -01:00",
"latitude": -86.57082,
"longitude": -29.981934,
"tags": [
  "commodo",
  "consectetur",
  "dolore",
  "voluptate",
  "proident",
  "Lorem",
  "non"
],
"friends": [
  {
    "id": 0,
    "name": "Lilia Stevenson"
  },
  {
    "id": 1,
    "name": "Moody Heath"
  },
  {
    "id": 2,
    "name": "Desiree Hayden"
  }
],
"greeting": "Hello, Romero Cervantes! You have 6 unread messages.",
"favoriteFruit": "banana"
}

jsonSample.append(record)




def randomJsonSample():
  # return one random json sample
  return copy.copy(jsonSample[random.randint(0, len(jsonSample) - 1)])

