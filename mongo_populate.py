#!/usr/bin/python
from array import array
import random
import sys
from threading import Thread, RLock
import time
import uuid

from pymongo import MongoClient
import datetime


import jsonSample as jsonsample


# Lock for shared multithreading vars
#lockFileAccessExample = RLock()

class StressTest(Thread):

  def __init__(self, repeatFactor):
    Thread.__init__(self)
    self.repeatFactor = repeatFactor

  def run(self):
    # Open connection one time by thread
    cli = MongoClient('mongodb://node1:27017,node2:27017,node3:27017/?replicaSet=adeliusrs')

    for i in range(0,self.repeatFactor):
#      with lockFileAccessExample:
          
      # Select database
      db = cli.adeliusDB
          
      # Select colections
      records_coll = db.records
      
      # Generate some random uuid
      record = jsonsample.randomJsonSample()
      record["guid"] = uuid.uuid4()
      #print record
      # Insert document
      records_coll.insert_one(record).inserted_id

            


###################################
# RUN
###################################

snapshotLaunchTimestamp = ( time.time() * 1000 )


threadMax = 50
repeatIteration = 80000

            
# Set dictionnay array of potential values
recordsSample = []

threads = []

for i in range(0,threadMax):
  # Create threads
  threads.append(StressTest(repeatIteration))
  # Run threads
  threads[i].start()
  print threads[i]

# Wait for all threads to end
for thread in threads:
    thread.join() # wait for completion


timeToComplete = int( ( time.time() * 1000 ) - snapshotLaunchTimestamp )
print "Script end in", timeToComplete, "milliseconds"
